//
grammar pydl;
//
@parser::header {
from pydl.IAccess import IAccess
from pydl.IBuiltin import IBuiltin
from pydl.IEdge import IEdge
from pydl.IFrame import IFrame
from pydl.IInherit import IInherit
from pydl.IMember import IMember
from pydl.ISequence import ISequence
from pydl.ISession import ISession
#
from pydl.helper import *
}
//
//  Program and options.
//
program
    :   option* (clsdef | proto)*
    ;
//
//  Options and related.
//
option
    :   include             {ISession().add_includes($include.txt)}
    |	'@prefix' STRING    {ISession().set_prefix($STRING.text[1:-1])}
        ';'
    |   nsptok n=namspc     {ISession().set_namespace($n.text)}
        ';'
    |   '@import'
        i=namspc p=STRING   {ISession().set_import($i.text,$p.text[1:-1])}
        ';'
    |   bastok intval       {set_base($intval.val)}
        ';'
    ;
nsptok
    :   '@namespace'
    |   d='namespace'       {print_deprecated($d.line,$d.pos,"namespace","@namespace")}
    ;
bastok
    :   '@base'
    |   d1='@protocol'      {print_deprecated($d1.line,$d1.pos,"@protocol","@base")}
    |   d2='protocol'       {print_deprecated($d2.line,$d2.pos,"protocol","@base")}
    ;
//
//  Class definitions.
//
clsdef
    :   clstok IDENT        {set_active($IDENT.text,$clstok.acs)}
        clsopts?
        inherits?
        '{'statement* '}'
        ';'
    ;
clstok  returns [acs=IAccess.PUBLIC]
    :   '@class'            {$acs=IAccess.PRIVATE}
    |   '@struct'
    |   d='class'           {print_deprecated($d.line,$d.pos,"class","@struct")}
    ;
clsopts
    :   clsopt clsopt*
    ;
clsopt
    :   '@smart'            {active().set_smart()}
    |   '@object'           {active().set_smart()}
        '[' typdef ']'
    |   '@default'
    ;
inherits
    :   ':' f=inherit       {active().add_inherit($f.inh)}
        ( ',' n=inherit     {active().add_inherit($n.inh)}
        )*
    ;
inherit returns [inh]
        @init {rem=False}
    :   acstok
        ('@extern'          {rem=True}
        )?
        namspc              {$inh=form_inherit($namspc.text,$acstok.acs,rem)}
    ;
statement
    :   member
        (code               {$member.mbr.set_init($code.cod)}
        )?
        (mbradp             {$member.mbr.set_adapt($mbradp.txt)}
        )?
        ';'
    |   acstok ':'          {active().set_access($acstok.acs)}
    |   code                {active().add_code($code.cod)}
    |   decl                {active().add_decl($decl.cod)}
    |   include             {active().add_include($include.txt)}
    ;
//
//  Type definitions.
//
typdef  returns [txt]
    :   namspc              {$txt=$namspc.text}
        ( tmplat            {$txt+=$tmplat.txt}
        )?
    ;
tmplat  returns [txt]
    :   '<' '>'             {$txt="<>"}
    |   '<' f=tmparg        {$txt="<"+$f.txt}
        ( ',' n=tmparg      {$txt+=',' + $n.txt}
        )* '>'              {$txt+='>'}
    ;
tmparg  returns [txt]
    :   typdef              {$txt=$typdef.txt}
    |   intval              {$txt=str($intval.val)}
    |   keytok              {$txt=$keytok.txt}
    |   valtok              {$txt="@val"}
    ;
keytok  returns [txt="@key"]
    :   '@key'
    |   d='%1%'             {print_deprecated($d.line,$d.pos,"%1%","@key")}
    ;
valtok  returns [txt="@val"]
    :   '@val'
    |   d='%2%'             {print_deprecated($d.line,$d.pos,"%2%","@val")}
    ;
//
//  Data members.
//
member  returns [mbr]
        locals [typ,key]
    :   namspc              {$typ=find_type($namspc.text)}
        IDENT               {$mbr=IMember($IDENT.text,active().access())}
        (mbrspc             {$typ=ISequence($typ,$mbrspc.txt)}
        )?                  {form_member($mbr,$typ)}
    |   '(' k=namspc ','    {$key=find_type($k.text)}
        v=namspc ')'        {$typ=find_type($v.text)}
        IDENT               {$mbr=IMember($IDENT.text,active().access())}
        mbrspc              {form_map($mbr,$key,$typ,$mbrspc.txt)}
    |   spctok IDENT        {$mbr=IMember($IDENT.text,active().access())}
        mbrspc              {form_space($mbr,$mbrspc.txt)}
    ;
mbrspc  returns [txt]
    :   '[' typdef ']'      {$txt=$typdef.txt}
    ;
mbradp  returns [txt]
    :   '@adapt' IDENT      {$txt=$IDENT.text}
    ;
spctok
    :   '@space'
    |   d='space'           {print_deprecated($d.line,$d.pos,"space","@space")}
    ;
//
//  Protocol definitions.
//
proto
        locals [prt]
    :   protok n=IDENT      {$prt=IProto($n.text)}
        bases[$prt]?
        '{'
        prtinc[$prt]*
        clnsrv[$prt]
        '}' ';'             {set_proto($prt)}
    ;
protok  returns [str="@protocol"]
    :   '@protocol'
    |   d='protocol'        {print_deprecated($d.line,$d.pos,"protocol","@protocol")}
    ;
clnsrv[prt]
    :   clnscp[$prt] srvspc[$prt]
    |   srvspc[$prt] clnscp[$prt]
    ;
clnscp[prt]
    :   client              {$prt.set_client($client.frm)}
    ;
srvspc[prt]
    :   server              {$prt.set_server($server.frm)}
    ;
client  returns [frm]
    :   clntok
        frame               {$frm=$frame.frm}
    ;
clntok
    :   '@client'
    |   d='client'          {print_deprecated($d.line,$d.pos,"client","@client")}
    ;
server  returns [frm]
    :   srvtok
        frame               {$frm=$frame.frm}
    ;
srvtok
    :   '@server'
    |   d='server'          {print_deprecated($d.line,$d.pos,"server","@server")}
    ;
frame   returns [frm=IFrame()]
    :   bases[$frm]?
        '{'
        prtinc[$frm]*
        states[$frm]
        initial[$frm]?
        (
        edge[$frm]
        ';' )* '}' ';'
    ;
bases[itm]
    :   ':' f=base          {$itm.set_base($f.inh)}
        ( ',' n=base        {$itm.set_base($n.inh)}
        )*
    ;
base returns [inh]
    :   acstok
        namspc              {$inh=IInherit($namspc.text,$acstok.acs,False)}
    ;
prtinc[itm]
    :   include             {$itm.set_include($include.txt)}
    ;
states[frm]
    :   statok
        '{' s=IDENT         {$frm.set_state($s.text)}
        ( ','
        t=IDENT             {$frm.set_state($t.text)}
        )*
        '}'
        trace[$frm]?
        ';'
    ;
statok
    :   '@states'
    |   d='states'          {print_deprecated($d.line,$d.pos,"states","@states")}
    ;
trace[frm]
    :   '@trace'            {$frm.set_trace('')}
    |   '@trace'
        '(' STRING ')'      {$frm.set_trace($STRING.text)}
    ;
initial[frm]
    :   initok IDENT ';'    {$frm.set_initial($IDENT.text)}
    ;
initok
    :   '@initial'
    |   d='initial'         {print_deprecated($d.line,$d.pos,"initial","@initial")}
    ;
edge[frm]
        locals [$edg]
    :   edgtok ':'
        (                   {$edg = IEdge()}
                            {$edg.set_state($edgtok.txt)}
          message '->'      {$edg.set_message($message.txt)}
          ( '^'             {$edg.set_raw(True)}
          )? a=IDENT        {$edg.set_call($a.text)}
          ( ',' n=IDENT     {$edg.set_next($n.text)}
          )?                {$frm.set_edge($edg)}
        )+
    |   '@close'
        IDENT               {$frm.set_edge(IEdge().set_exception('ppr::cClose',$IDENT.text))}
    |   '@collapse'
        IDENT               {$frm.set_edge(IEdge().set_exception('ppr::cCollapse',$IDENT.text))}
    ;
edgtok  returns [txt]
    :   IDENT               {$txt=$IDENT.text}
    |   '@'                 {$txt="@"}
    ;
message returns[txt]
    :   namspc              {$txt=$namspc.text}
    |                       {$txt='@'}
    ;
//
//  Access tokens.
//
acstok  returns [acs]
    :   '@private'          {$acs=IAccess.PRIVATE}
    |   '@public'           {$acs=IAccess.PUBLIC}
    |   '@protected'        {$acs=IAccess.PROTECTED}
    ;
//
//  Include statements.
//
include returns [txt]
    :   INCSTR              {$txt=strip_include($INCSTR.text)}
    |   INCBRK              {$txt=strip_include($INCBRK.text)}
    ;
//
//  Literal blocks.
//
decl    returns [cod]
    :   DECL                {$cod=$DECL.text[2:-2]}
    ;
code    returns [cod]
    :   CODE                {$cod=$CODE.text[2:-2]}
    ;
//
//  Numeric values.
//
intval  returns [val]
    :   INT                 {$val = int($INT.text)}
    |   HEX                 {$val = int($HEX.text[2:],16)}
    |   OCT                 {$val = int($OCT.text[1:],8)}
    ;
fltval  returns [val]
    :   FLOAT               {$val = float($FLOAT.text)}
    ;
//
//  Identifiers.
//
namspc
    :   IDENT ('::' IDENT)*
    ;
//
///////////////////////////////////////////////////////////////////////////////
//  Lexical elements.
//
IDENT : [a-zA-Z_][0-9a-zA-Z_]*
    ;
//
//  String literals.
//
INCBRK : '#include' [ \t\r\n]+ '<' Chr+ '>';
INCSTR : '#include' [ \t\r\n]+ '"' Chr+ '"';
STRING : '"' Chr* '"';
fragment EscSeq
    :   SmpEsc
    |   OctEsc
    |   HexEsc
    |   UniEsc
    ;
fragment SmpEsc : '\\' ['"?abfnrtv\\];
fragment OctEsc : '\\' OctDgt OctDgt? OctDgt?;
fragment HexEsc : '\\x' HexDgt+;
fragment UniEsc
    :   '\\u' HexQuad
    |   '\\U' HexQuad HexQuad
    ;
fragment HexQuad : HexDgt HexDgt HexDgt HexDgt;
fragment Chr
    :   ~["\\\r\n]
    |   EscSeq
    |   '\\\n'                                          // Added line
    |   '\\\r\n'                                        // Added line
    ;
//
//  Integral literal.
//
INT	: ('-' | '+')? ('0'..'9')+;
OCT	: '0' OctDgt+;
fragment OctDgt : [0-7];
HEX	: '0x' HexDgt+;
fragment HexDgt : [0-9a-fA-F];
//
//  Floating point literal
//
FLOAT
    :   ('0'..'9')+ '.' ('0'..'9')* Exponent?
    |   '.' ('0'..'9')+ Exponent?
    |   ('0'..'9')+ Exponent
	;
//
fragment Exponent : ('e'|'E') ('+'|'-')? ('0'..'9')+ ;
//
//  Literal captures.
//
DECL : '#{' .*? '#}';
CODE : '@{' .*? '@}';
//
//  Whitespace and comments.
//
BLOCKCOMMENT : '/*' .*? '*/' -> skip;
LINECOMMENT : '//' ~[\r\n]* -> skip;
WS : [ \t\r\n]+ -> skip ;
