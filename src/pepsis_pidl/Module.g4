/*
 *	Copyright (C) 2023	    Roger March
 *
 *	See the file 'COPYING' for license information.
 */
grammar Module;
//
@parser::header {
}
//
//  Start of parser.
//
program
    :   space element*
	;
space
    :   NAMESPACE typstr ';'
	;
element
    :	IDENT ( SMART | objdef | FIXED | STRING )* ';'
	;
objdef
	:	'@object' '[' type ']'
	;
typstr
	:	IDENT ( '::' IDENT )*
	;
type
	:	tname ( '::' tname )*
	;
tname
	:	IDENT ( tmpl )?
	;
tmpl
	:	'<' ( tmty ( ',' tmty )* )? '>'
	;
tmty
	:	type
	|	INT
	|	('%1%' | '%2%')
	;
//
//	Keywords.
//
NAMESPACE   :	'@namespace' ;
SMART       :	'@smart' ;
FIXED       :	'@fixed' ;
STRING	    :	'@string' ;
//
//	Paths.
//
IPATH
    :	'<' PATHEL+ '>'
	|	'"' PATHEL+ '"'
	;
fragment PATHEL	:	'0'..'9' | 'a'..'z' | 'A'..'Z' | '_' | '-' | '.' | '/'
	;
//
//  Lexical elements.
//
IDENT : [a-zA-Z_][0-9a-zA-Z_]*;
INT	: ('-' | '+')? ('0'..'9')+;
//
//  Whitespace and comments.
//
BLOCKCOMMENT : '/*' .*? '*/' -> skip;
LINECOMMENT : '//' ~[\r\n]* -> skip;
WS : [ \t\r\n]+ -> skip ;
