/*
 *	Copyright (C) 2023  	Roger March
 *
 *	See the file 'COPYING' for license information.
 */
grammar Pidl;
//
@parser::header {
}
//
//      The parser.
//
program
    :   option* (clsdef | proto | module)*
	;
option
    :	incpath
	|	PREFIX STRING ';'?
	|	'namespace' namespc ';'
	|	PROTOCOL intval ';'
	;
namespc
	:   IDENT ('::' IDENT)*
	;
incpath
	:	BPATH
	|	QPATH
	;
//
//      Message classes.
//
clsdef
    :	'class' IDENT clsopt* '{' statement* '}' ';'
	;
clsopt
    :	'@smart'
	|   '@object' '[' type ']'
	|   '@default'
	;
member
    :	typstr IDENT ('[' type ']')?
	|	'(' typstr ',' typstr ')' IDENT '[' type ']'
	|	'space' IDENT '[' type ']'
	;
statement
    :   member ('@adapt' IDENT)? ';'
	|	CODE
	|	DECL
	|	incpath
	;
typstr
	:	IDENT ('::' IDENT)*
	;
type
	:	tname ('::' tname)*
	;
tname
	:	IDENT tmpl?
	;
tmpl
	:	'<' (tmty (',' tmty)* )? '>'
	;
tmty
	:	type
	|	INT
	|	'%1%'
	|   '%2%'
	;
//
//      Modules (these should be deprecated I believe).
//
module
    :   MODULE typstr '{' (PREFIX STRING ';')? impcls* '}' ';'
	|	MODULE STRING ';'
	;
impcls
    :   'class' IDENT impopt? ';'
	;
impopt
    :	'@smart'
	|   '@object' '[' type ']'
	;
//
//  Protocols.
//
proto
    :	'protocol' n=IDENT ( '[' namespc ']' )? '{' prtincs* client server '}' ';'
	;
prtincs
    :	incpath
	;
client
    :	'client' ('@timeout' '(' fltval ')')? frame
	;
server
    :	'server' frame
	;
frame
    :	'[' namespc (',' namespc)? ']' '{' frmincs* states initial? rule* '}' ';'
	;
frmincs
    :	incpath
	;
states
    :	'states' '{' IDENT (',' IDENT)* '}' ( '@trace' | '@trace' '(' STRING ')' )? ';'
	;
initial
    :	'initial' IDENT';'
	;
rule
	:	(IDENT | '@') ':' (typstr? '->' '^'? IDENT (',' IDENT)? )+ ';'
	|	'@close' IDENT ';'
	|	'@collapse' IDENT ';'
	|	'@timeout' IDENT ';'
	;
//
//  Numeric values.
//
intval  returns [val]
    :   INT                 {$val = int($INT.text)}
    |   HEX                 {$val = int($HEX.text[2:],16)}
    |   OCT                 {$val = int($OCT.text[1:],8)}
    ;
fltval  returns [val]
    :   FLOAT               {$val = float($FLOAT.text)}
    ;
//
//      Keywords.
//
MODULE      :   '@module' ;
PREFIX	    :	'@prefix' ;
PROTOCOL    :	'@protocol' ;
//
//      Identifiers.
//
IDENT : [a-zA-Z_][0-9a-zA-Z_]* ;
//
//      String literals.
//
BPATH : '#include' [ \t\r\n]+ '<' Chr+ '>';
QPATH : '#include' [ \t\r\n]+ '"' Chr+ '"';
STRING : '"' Chr* '"';
fragment EscSeq
    :   SmpEsc
    |   OctEsc
    |   HexEsc
    |   UniEsc
    ;
fragment SmpEsc : '\\' ['"?abfnrtv\\];
fragment OctEsc : '\\' OctDgt OctDgt? OctDgt?;
fragment HexEsc : '\\x' HexDgt+;
fragment UniEsc
    :   '\\u' HexQuad
    |   '\\U' HexQuad HexQuad
    ;
fragment HexQuad : HexDgt HexDgt HexDgt HexDgt;
fragment Chr
    :   ~["\\\r\n]
    |   EscSeq
    |   '\\\n'                                          // Added line
    |   '\\\r\n'                                        // Added line
    ;
//
//      Integral literal.
//
INT	: ('-' | '+')? ('0'..'9')+;
OCT	: '0' OctDgt+;
fragment OctDgt : [0-7];
HEX	: '0x' HexDgt+;
fragment HexDgt : [0-9a-fA-F];
//
//      Floating point literal
//
FLOAT
    :   ('0'..'9')+ '.' ('0'..'9')* Exponent?
    |   '.' ('0'..'9')+ Exponent?
    |   ('0'..'9')+ Exponent
	;
//
fragment Exponent : ('e'|'E') ('+'|'-')? ('0'..'9')+ ;
//
//      Literal captures.
//
DECL : '#{' .*? '#}';
CODE : '@{' .*? '@}';
//
//      Whitespace and comments.
//
BLOCKCOMMENT : '/*' .*? '*/' -> skip;
LINECOMMENT : '//' ~[\r\n]* -> skip;
WS : [ \t\r\n]+ -> skip ;
