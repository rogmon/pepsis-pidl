[![Project generated with PyScaffold](https://img.shields.io/badge/-PyScaffold-005CA0?logo=pyscaffold)](https://pyscaffold.org/)

# pepsis-pidl

> Python implementation of the Pepsis **pidl** program.

This exists because the C++ version uses an ANTLR3 parser.
Its code is so old that it can not be moved to C++20.
Hence, this **pidl** is an ANTLR4, **python** based, stop gap until **pydl** is completed.

## Bootstrap Notes:

If you are reading this you probably already know your Gitlab repository.
Just in case you don't here is how to get a clone:

| Type     | Command                                                      |
| -------- | ------------------------------------------------------------ |
| Public   | git clone https://gitlab.com/rogmon/pepsis-pidl.git          |
| Holodeck | git clone http://frqstic.s.cb.icmanage.com/roger/pepsis-pidl.git |

### Preliminaries

From now on all your work should be in the *pepsis-pidl* directory.
The **PyCharm** IDE is used for development.
To do the same create a new python project for *pepsis-pidl,* selecting it to use existing source[^1].
Set it up to use a **venv** environment, it will be customized in a bit.

It is highly recommended that use **pyenv** to supply your **python**.
This way you can easily test against different versions.
The minimal requirement for **pepsis-pidl** is **python-3.7.1**[^2].
Check out https://realpython.com/intro-to-pyenv for hints on the operating system requirements.
Here is how to get the interpreter up:

```bash
curl https://pyenv.run | bash           # makes .bashrc changes.
exec bash                               # to pick up the .bashrc changes.
pyenv install 3.7.1                     # build the specific python version.
pyenv virtualenv 3.7.1 pepsis-pidl      # define a named version for the project.
```

Bind the **venv** the named interpreter.
In **PyCharm**:

```
File -> settings -> Project: pepsis-pidl -> Python Interpreter
```

add a new interpreter, it will be local.
Its path should be:

```
~/.pyenv/versions/pepsis-pidl/bin/python
```

Now populate the **venv** with packages:

```bash
pip install -e .                        # project needs.
pip install tox                         # for builds and publishing.
```

### ANTLR 4

If you wish to play with the grammar in **PyCharm**, install the **Antlr4** plugin from the marketplace.
Then select the file *src/pepsis_pidl/Pidl.g4*.
Configure Antlr4 from the title menu:

```
Tools -> Configure ANTLR...
```

In the pop up, fill in the fields:

| Field                     | Value                           |
| ------------------------- | ------------------------------- |
| Output directory ...      | .../pepsis-pidl/src/pepsis_pidl |
| Grammar file encoding ... | utf-8                           |
| Package/namespace ...     | generated                       |
| Language ...              | Python3                         |
| Case transformation       | Leave as-is                     |

There are also two check-boxes, make sure the listener is  selected and the visitor is not.
You may also wish to select the check-box at the top to auto generate the parser on save.

### Development

For **PyCharm** there are a few more settings to make.
Some directories want to be marked so the IDE understands how to deal with them.
Select the pop up:

```
File -> settings -> Project: pepsis-pidl -> Project Structure
```

and make the directories as:

| Directory                | Type      |
| ------------------------ | --------- |
| src/pepsis_pidl          | Sources   |
| tests                    | Tests     |
| venv                     | Excluded  |
| dist                     | Excluded  |
| src/pepsis_pidl.egg-info | Excluded  |
| docs                     | Resources |



### Building

To do a build:

```bash
tox -e build
```

The result will be in the *.../dist* directory.
Normally this will create development build with all sorts of suffixing on the file names.
To build for release you need to check in all your changes and then add a git release tag.
This has the form **major**.**minor**.[**tweak**], as in 1.5 or 1.5.9.

To publish to the PYPI test repository:

```
tox -e publish
```

and to the release repository:

```
tox -e publish -- --repository pypi
```

You will need an account for this[^3].

<!-- pyscaffold-notes -->

## Note

This project has been set up using **PyScaffold** 4.3.1. For details and usage information on **PyScaffold** see https://pyscaffold.org/.

[^1]: It has an annoying box to add a *main.py* file, uncheck that.\
[^2]: At least that was what it was developed against.\
[^3]: To do this on your own you will probably have to rename the project to not conflict with official releases.\



